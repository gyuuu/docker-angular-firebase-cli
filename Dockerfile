FROM node:8.2.1

LABEL maintainer "György Pusker <gpusker89@gmail.com>"

RUN mkdir /home/.npm-global
ENV PATH=/home/.npm-global/bin:$PATH
ENV NPM_CONFIG_PREFIX=/home/.npm-global
RUN chown node:node /home/.npm-global
USER node
# Install node dependencies
RUN npm install --quiet --no-progress -g @angular/cli@latest firebase-tools
RUN npm cache clean --force
